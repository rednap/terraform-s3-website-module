# Very simple (one input) s3 website module
  
pass the input var "bucket_name" and it creates an s3 bucket which works as a website.   
  
it outputs two vars, "website_endpoint" and "website_bucket_id.  
website_endpoint is the URL you can use to visit the site in a browser  
website_bucket_id is the bucket name, used for other resources which depend on your bucket  
